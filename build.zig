const std = @import("std");
const builtin = @import("builtin");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{ });

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "genetics-game",
        // In this case the main source file is merely a path, however, in more
        // complicated build scripts, this could be a generated file.
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    const sdl = @import("sdl");
    const sdk = sdl.init(b, null);
    sdk.link(exe, .dynamic);
    sdk.linkTtf(exe);
    exe.addModule("sdl2", sdk.getWrapperModule());

    // dvui
    const dvui_dep = b.dependency("dvui", .{ .target = target, .optimize = optimize });
    exe.addModule("dvui", dvui_dep.module("dvui"));
    exe.addModule("SDLBackend", dvui_dep.module("SDLBackend"));
    link_deps(exe, dvui_dep);

    // This declares intent for the executable to be installed into the
    // standard location when the user invokes the "install" step (the default
    // step when running `zig build`).
    b.installArtifact(exe);

    // This *creates* a Run step in the build graph, to be executed when another
    // step is evaluated that depends on it. The next line below will establish
    // such a dependency.
    const run_cmd = b.addRunArtifact(exe);

    // By making the run step depend on the install step, it will be run from the
    // installation directory rather than directly from within the cache directory.
    // This is not necessary, however, if the application depends on other installed
    // files, this ensures they will be present and in the expected location.
    run_cmd.step.dependOn(b.getInstallStep());

    // This allows the user to pass arguments to the application in the build
    // command itself, like this: `zig build run -- arg1 arg2 etc`
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    // This creates a build step. It will be visible in the `zig build --help` menu,
    // and can be selected like this: `zig build run`
    // This will evaluate the `run` step rather than the default, which is "install".
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    // Creates a step for unit testing. This only builds the test executable
    // but does not run it.
    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    sdk.link(unit_tests, .dynamic);
    sdk.linkTtf(unit_tests);
    unit_tests.addModule("sdl2", sdk.getWrapperModule());

    const run_unit_tests = b.addRunArtifact(unit_tests);

    // Similar to creating the run step earlier, this exposes a `test` step to
    // the `zig build --help` menu, providing a way for the user to request
    // running the unit tests.
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}



fn link_deps(exe: *std.Build.Step.Compile, dvui_dep: *std.Build.Dependency) void {
    const b = dvui_dep.builder;

    //This should all be cleaned up when zig 0.12.x is released.
    if (true) {
        // Here we are trying to work within zig 0.11.x but not expose dvui's
        // internal dependencies.
        exe.linkLibrary(dvui_dep.artifact("dvui_libs"));
        @import("root").dependencies.imports.dvui.add_include_paths(dvui_dep.builder, exe);
    } else {
        // Here we link the internal dependencies explicitly.

        // TODO: remove this part about freetype (pulling it from the dvui_dep
        // sub-builder) once https://github.com/ziglang/zig/pull/14731 lands

        const freetype_dep = b.dependency("freetype", .{
            .target = exe.target,
            .optimize = exe.optimize,
        });
        exe.linkLibrary(freetype_dep.artifact("freetype"));

        // TODO: remove this part about stb_image once either:
        // - zig can successfully cimport stb_image.h
        // - zig can have a module depend on a c file
        const stbi_dep = b.dependency("stb_image", .{
            .target = exe.target,
            .optimize = exe.optimize,
        });
        exe.linkLibrary(stbi_dep.artifact("stb_image"));
    }

    exe.linkLibC();

    if (exe.target.isWindows()) {
        const sdl_dep = b.dependency("sdl", .{
            .target = exe.target,
            .optimize = exe.optimize,
        });
        exe.linkLibrary(sdl_dep.artifact("SDL2"));

        exe.linkSystemLibrary("setupapi");
        exe.linkSystemLibrary("winmm");
        exe.linkSystemLibrary("gdi32");
        exe.linkSystemLibrary("imm32");
        exe.linkSystemLibrary("version");
        exe.linkSystemLibrary("oleaut32");
        exe.linkSystemLibrary("ole32");
    } else {
        if (exe.target.isDarwin()) {
            exe.linkSystemLibrary("z");
            exe.linkSystemLibrary("bz2");
            exe.linkSystemLibrary("iconv");
            exe.linkFramework("AppKit");
            exe.linkFramework("AudioToolbox");
            exe.linkFramework("Carbon");
            exe.linkFramework("Cocoa");
            exe.linkFramework("CoreAudio");
            exe.linkFramework("CoreFoundation");
            exe.linkFramework("CoreGraphics");
            exe.linkFramework("CoreHaptics");
            exe.linkFramework("CoreVideo");
            exe.linkFramework("ForceFeedback");
            exe.linkFramework("GameController");
            exe.linkFramework("IOKit");
            exe.linkFramework("Metal");
        }

        exe.linkSystemLibrary("SDL2");
        //exe.addIncludePath(.{.path = "/Users/dvanderson/SDL2-2.24.1/include"});
        //exe.addObjectFile(.{.path = "/Users/dvanderson/SDL2-2.24.1/build/.libs/libSDL2.a"});
    }
}

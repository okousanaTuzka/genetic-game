const std = @import("std");
const math = @import("std").math;


pub fn addAngleRad(angle1: f32, angle2: f32) f32 {
    var result = angle1 + angle2;
    if (result >= math.pi) result -= math.pi * 2;
    if (result < -math.pi) result += math.pi * 2;

    return result;
}

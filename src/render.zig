const std = @import("std");
const math = std.math;
const SDL = @import("sdl2");
const sim = @import("sim.zig");

pub const RendererSys = struct {

    renderer: Renderer,

    pub fn new(renderer: Renderer) @This() {
        return RendererSys{
            .renderer = renderer,
        };
    }

    pub fn render(this: @This(), delta_time: f32, entities: *const std.ArrayList(sim.base)) !void {
        _ = delta_time;

        const selected_line = [_][2]f32{
            [_]f32{ 2,  0 },
            [_]f32{ 1.4, 1.4 },
            [_]f32{ 0,  2 },
            [_]f32{-1.4, 1.4 },
            [_]f32{-2,  0 },
            [_]f32{-1.4, -1.4 },
            [_]f32{ 0, -2 },
            [_]f32{ 1.4, -1.4 },
            [_]f32{ 2,  0 },
        };

        const shape = [_][2]f32{ [_]f32{-1, -1 }, [_]f32{-1,  1 }, [_]f32{ 2,  0 }};
        for (entities.items) |*entity| {
            const size_scale = entity.size;
            try this.renderer.renderShape(entity.x, entity.y, entity.dir_rad, size_scale, &shape);

            if (entity.selected) {
                try this.renderer.renderLines(entity.x, entity.y, entity.dir_rad, size_scale, &selected_line);
                try this.debug(entity);
            }
        }
    }

    fn debug(this: @This(), entity: *sim.base) !void {

        inline for (&(entity.net.layers), 0..) |*layer, i| {
            for (0..layer.*.neuron_count) |neuron_i| {
                for (0..layer.*.input_count) |input_i| {

                    const line_height = 32;
                    const xoff: i32 = @intCast(i * 128);
                    const yoff: i32 = @intCast(neuron_i * layer.*.input_count * line_height + neuron_i * line_height + input_i * line_height);

                    try this.renderer.renderText(3500 + xoff, 32 + yoff, "{d:3.1}", .{ layer.*.neurons[neuron_i][input_i] });

                }
            }
        }
    }
};

pub const Renderer = struct {

    font: SDL.ttf.Font,
    renderer: SDL.Renderer,

    pub fn new(renderer: SDL.Renderer) !@This() {
        return Renderer{
            .font = try SDL.ttf.openFont("/usr/share/fonts/TTF/ProggyVector Regular.ttf", 32),
            .renderer = renderer,
        };
    }

    pub fn clear(this: @This()) !void {
        try this.renderer.setColorRGB(0, 0, 0);
        try this.renderer.clear();
    }

    pub fn show(this: @This()) void {
        this.renderer.present();
    }

    pub fn renderShape(this: @This(), x: f32, y: f32, rotation: f32, scale: f32, shape: []const[2]f32) !void {
        try std.testing.expect(shape.len == 3);

        try this.renderer.setColorRGB(0, 0, 0);

        try this.renderer.setColor(SDL.Color.parse("#F7A41D") catch unreachable);

        const c = SDL.Color.white;
        var v: [3]SDL.Vertex = undefined;
        for (shape, 0..) |point, i| {
            v[i] = .{ .position = move(x, y, rotate(rotation,  point[0] * scale, point[1] * scale)), .color = c };
        }

        try this.renderer.drawGeometry( null, &v, null );
    }

    pub fn renderText(this: @This(), x: i32, y: i32, comptime fmt_text: [:0]const u8, args: anytype) !void {
        var buffer: [128:0]u8 = undefined;
        const text = try std.fmt.bufPrintZ(&buffer, fmt_text, args);

        const surface = try this.font.renderTextSolid(text, SDL.Color.green);
        defer surface.destroy();

        const texture = try SDL.createTextureFromSurface(this.renderer, surface);
        defer texture.destroy();

        const h = 32;
        const w: i32 = @intCast(text.len * 16);

        var rec = SDL.Rectangle{ .x = x, .y = y, .width = w, .height = h };
        try this.renderer.copy(texture, rec, null);
    }

    pub fn renderLines(this: @This(), x: f32, y: f32, rotation: f32, scale: f32, lines: []const[2]f32) !void {
        try std.testing.expect(lines.len > 2);

        try this.renderer.setColorRGB(0, 255, 0);

        for (0..lines.len-1) |i| {
            const p1 = move(x, y, rotate(rotation,  lines[i][0] * scale, lines[i][1] * scale));
            const p2 = move(x, y, rotate(rotation,  lines[i+1][0] * scale, lines[i+1][1] * scale));
            try this.renderer.drawLineF(p1.x, p1.y, p2.x, p2.y);
        }

    }

};


const pos = struct {
    x: f32,
    y: f32,
};

fn rotate(angle: f32, x: f32, y: f32) pos {

    const cos = math.cos(angle);
    const sin = math.sin(angle);

    return .{
        .x = cos * x - sin * y,
        .y = sin * x + cos * y,
    };
}

fn move(x: f32, y: f32, pos_: pos) SDL.PointF {
    return .{
        .x = pos_.x + x,
        .y = pos_.y + y,
    };
}

test "rotate" {
    try std.testing.expectApproxEqAbs(@as(f32, -1), rotate(math.pi, 1 , 0).x, 0.001);
    try std.testing.expectApproxEqAbs(@as(f32, -0), rotate(math.pi, 1 , 0).y, 0.001);
    try std.testing.expectApproxEqAbs(@as(f32, -0), rotate(math.pi, 0 , 1).x, 0.001);
    try std.testing.expectApproxEqAbs(@as(f32, -1), rotate(math.pi, 0 , 1).y, 0.001);
}

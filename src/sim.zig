const std = @import("std");
const math = @import("std").math;
const Vec2 = @import("vector.zig").Vec2;
const perf = @import("perf.zig");
const nn = @import("nn.zig");
const genetics = @import("genetics.zig");
const utils = @import("utils.zig");
const Collision = @import("systems/collisions.zig").Collision;

const MAX_ROTATION = 3.14;
const MAX_VELOCITY = 100;
const SIGHT_DISTANCE = 50;

const netType = nn.NeuralNetwork(&[_]usize{6, 4, 4}, &[_]nn.Activation{.tanh, .tanh});

pub const base = struct {
    id: usize = ~@as(usize, 0),
    size: f32 = 10,
    x: f32 = 0,
    y: f32 = 0,
    dir_rad: f32 = 0,
    vel: f32 = 0,
    rotation: f32 = 0,
    net: netType = .{},
    life_time: f32 = 0,
    score: f32 = 0,
    feed_timer: f32 = 0,
    memory: [2]f16 = [_]f16{0, 0},
    selected: bool = false,
};

pub fn updateEntityIds(entities: *std.ArrayList(base)) void {
    for (entities.items, 0..) |*entity, id| {
        entity.id = id;
    }
}

pub const MovementSys = struct {

    width: f32,
    height: f32,

    pub fn calcMovement(this: *@This(), delta_time: f32, entities: *std.ArrayList(base)) !void {

        for (entities.items) |*entity| {
            entity.dir_rad = utils.addAngleRad(entity.dir_rad, entity.rotation * delta_time);

            var v = Vec2.new(entity.vel, 0);
            v.rotate(entity.dir_rad);
            entity.x += v.x * delta_time;
            entity.y += v.y * delta_time;

            if (entity.x >= this.width)  entity.x -= this.width;
            if (entity.x < 0)            entity.x += this.width;
            if (entity.y >= this.height) entity.y -= this.height;
            if (entity.y < 0)            entity.y += this.height;
        }

    }
};

pub const BrainSys = struct {

    width: f32,
    height: f32,

    pub fn calcDecision(this: *@This(), delta_time: f32, entities: *std.ArrayList(base), collisions: *const std.ArrayList(Collision)) !void {
        _ = delta_time;
//         _ = collisions;

        var vis_inputs: [10_000][2]f16 = [_][2]f16{ [_]f16{0, 0}} ** 10_000;
        std.testing.expect( entities.items.len < vis_inputs.len ) catch unreachable;
        calcVision(collisions, &vis_inputs);

        var center = Vec2.new(this.width / 2, this.height / 2);
        for (entities.items) |*entity| {
            var v = Vec2.new(entity.x, entity.y);
            var dist = v.distanceTo(@constCast(&center));

            const input = [_]f16{@floatCast(dist.size() / center.x), @floatCast(utils.addAngleRad(dist.angle(), entity.dir_rad)), vis_inputs[entity.id][0], vis_inputs[entity.id][1], entity.memory[0], entity.memory[1]};

            const res = entity.net.calcInput(input);

            entity.vel = MAX_VELOCITY * res[0];
            entity.rotation = MAX_ROTATION * res[1];
            entity.memory[0] = res[2];
            entity.memory[1] = res[3];
        }

    }

    fn calcVision(collisions: *const std.ArrayList(Collision), inputs: [][2]f16) void {
        for (collisions.items) |*collision| {
            if (collision.dir.size() < SIGHT_DISTANCE) {
                const dist: f16 = @floatCast(@fabs(collision.dir.size() / SIGHT_DISTANCE));
                const dir1: f16 = @floatCast(utils.addAngleRad(collision.entity1.dir_rad, collision.dir.angle()));
                const dir2: f16 = @floatCast(utils.addAngleRad(collision.entity2.dir_rad, utils.addAngleRad(collision.dir.angle(), -math.pi)));
                if (@fabs(dir1) < math.pi / 10.0 and dist < inputs[collision.entity1.id][0]) {
                    inputs[collision.entity1.id][0] = dist;
                    inputs[collision.entity1.id][1] = dir1;
                }
                if (@fabs(dir2) < math.pi / 10.0 and dist < inputs[collision.entity2.id][0]) {
                    inputs[collision.entity2.id][0] = dist;
                    inputs[collision.entity2.id][1] = dir2;
                }
            }
        }
    }
};

pub const ScoreSys = struct {

    width: f32,
    height: f32,
    max_population: usize,

    pub fn update(this: *@This(), delta_time: f32, entities: *std.ArrayList(base), collisions: *const std.ArrayList(Collision)) !void {

        handleColissions(collisions);

        for (entities.items) |*entity| {
            entity.feed_timer += delta_time;

            var v = Vec2.new(entity.x, entity.y);
            var center = Vec2.new(this.width / 2, this.height / 2);
            var dist = v.distanceTo(@constCast(&center));

            const limit = 800;
            const steepness = 200;
            entity.score += 0.2 * @max(-0, @min(0,  ((-dist.size() + limit) / steepness)) + 1) * delta_time;
            //entity.score += 0.1 * @min(1, (@fabs(entity.vel) - 0 ) * 0.1) * delta_time;
//             entity.score = @max(0, entity.score);
            entity.score -= 0.4 * (@fabs(entity.vel) / MAX_VELOCITY) * delta_time;
            entity.score -= 0.2 * (@fabs(entity.rotation) / MAX_ROTATION) * delta_time;

            const pop_size = entities.items.len;

            const population_ratio = @as(f32, @floatFromInt(pop_size)) / (@as(f32, @floatFromInt(this.max_population)));

            if (entity.score > 5 + population_ratio * 6  and entities.items.len < this.max_population) {
                const new_net = genetics.createChild(netType, &(entity.net));

                entity.score = 0;

                var new_ent = base {
                    .size = 5,
                    .x = entity.x,
                    .y = entity.y,
                    .dir_rad = -entity.dir_rad,
                    .net = new_net,
                };
                entities.append(new_ent) catch unreachable;
            }
        }

        updateEntityIds(entities);

    }

    fn handleColissions(collisions: *const std.ArrayList(Collision)) void {
        const time_limit = 0.5;
        const score = 2.5;

        for (collisions.items) |col| {
            const a = col.entity1.size + col.entity2.size;
            if (col.distance < a) {

//                 std.log.err("{d:.1} {d:.1} {d:.1}", .{col.entity1.vel, col.entity1.feed_timer, col.dir.angle() - col.entity1.dir_rad});

                if (col.entity1.vel > 0 and col.entity1.feed_timer > time_limit and @cos(col.dir.angle() - col.entity1.dir_rad) > 0.5) {
                    col.entity1.feed_timer = 0;
                    const points: f32 = score * col.entity1.vel / MAX_VELOCITY;
                    col.entity1.score += points;
                    col.entity2.score -= points;
                }

                if (col.entity2.vel > 0 and col.entity2.feed_timer > time_limit and @cos(col.dir.angle() + std.math.pi - col.entity2.dir_rad) > 0.5) {
                    col.entity2.feed_timer = 0;
                    const points: f32 = score * col.entity2.vel / MAX_VELOCITY;
                    col.entity1.score -= points;
                    col.entity2.score += points;
                }

            }
        }
    }

};

pub const LifeTimeSys = struct {

    pub fn update(this: *@This(), delta_time: f32, entities: *std.ArrayList(base)) !void {
        _ = this;

        const len = entities.items.len;
        for (entities.items, 0..) |_, x| {
            const i = len - 1 - x;  // iterate in reverse oreder because then we can remove during iteration
            if (entities.items.ptr[i].selected) {
                continue;
            }
            entities.items.ptr[i].life_time += delta_time;
            if (entities.items.ptr[i].score < 0) entities.items.ptr[i].life_time -= entities.items.ptr[i].score / 4;
            if (entities.items.ptr[i].life_time > 60) {
                _ = entities.swapRemove(i);
            }
        }
        updateEntityIds(entities);
    }
};

test "cast" {
    const a: usize = 10;
    const b: f32 = 3;

    std.log.err("{d}", .{ a / b });
    std.log.err("{d}", .{@mod(-10.3, 3.0)});

    var x : usize = @intFromFloat(@as(f32, 0.000000001));
    std.log.err("{}", .{ x });

    x = @intFromFloat(@as(f32, 0.00097 / 200.0));
    std.log.err("{}", .{ x });


}

const std = @import("std");
const nn = @import("nn.zig");

var r = std.rand.DefaultPrng.init(13);
const rand = r.random();

pub fn createChild(comptime T: type,  net: *T) T {
    var child = T{};
    @memcpy(@as(*[1]T, &child), @as(*[1]T, net));

    mutateAll(T, &child);

    return child;
}

fn mutateSome(comptime T: type,  net: *T) void {
    for (0..1) |_| {
        const layer_index = rand.uintAtMost(usize, net.layers.len);
        var neuron_index: usize = undefined;
        var input_index: usize = undefined;

        inline for (&(net.layers), 0..) |*layer, i| {
            if (i == layer_index) {
                neuron_index = rand.uintLessThan(usize, layer.*.neuron_count);
                input_index = rand.uintAtMost(usize, layer.*.input_count);

                var new_weight = layer.*.neurons[neuron_index][input_index];
                const change_scale = 0.5 * (rand.float(f32) - 0.5);
                const change_value = 0.25 * (rand.float(f32) - 0.5);
                new_weight += new_weight * @as(f16, @floatCast(change_scale));
                new_weight += @floatCast(change_value);
                new_weight = @min(3, @max(-3, new_weight));
//                 std.log.err("old {d:5.3} new {d:5.3}", .{ layer.*.neurons[neuron_index][input_index], new_weight} );
                layer.*.neurons[neuron_index][input_index] = new_weight;
            }
        }
    }
}

fn mutateAll(comptime T: type,  net: *T) void {

    inline for (&(net.layers)) |*layer| {
        for (0..layer.*.neuron_count) |neuron_i| {
            for (0..layer.*.input_count) |input_i| {

                var new_weight = layer.*.neurons[neuron_i][input_i];
                const change_scale = 0.25 * (rand.float(f32) - 0.5);
                const change_value = 0.1 * (rand.float(f32) - 0.5);
                new_weight += new_weight * @as(f16, @floatCast(change_scale));
                new_weight += @floatCast(change_value);
                new_weight = @min(3, @max(-3, new_weight));
//                 std.log.err("old {d:5.3} new {d:5.3}", .{ layer.*.neurons[neuron_index][input_index], new_weight} );
                layer.*.neurons[neuron_i][input_i] = new_weight;
            }
        }
    }
}

test "createChild" {

//     const nn_type = nn.NeuralNetwork(&[_]usize{2, 2, 1}, &[_]nn.Activation{.identity, .identity});
//
//     const a = nn_type{};
//     var l = @constCast(&a);
//
//     var input = [_]f16{0.5, 0.2};
//
//     l.setWeights(1, 0, &[_]f16{0, 1, 1});
//     l.setWeights(1, 1, &[_]f16{1, 0.5, 0.5});
//
//     l.setWeights(2, 0, &[_]f16{0.5, 0.5, 1});
//
//     var child = createChild(nn_type, l);
//
//     try std.testing.expectEqualSlices(f16, &l.calcInput(input), &child.calcInput(input));
}


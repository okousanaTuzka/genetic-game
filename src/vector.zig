const std = @import("std");
const math = std.math;
const testing = std.testing;

pub const Vec2 = struct {
    x: f32,
    y: f32,

    pub fn new(x: f32, y: f32) @This() {
        var a = Vec2{ .x = x, .y = y };
        return a;
    }

    pub fn rotate(this: *@This(), angle_: f32) void {
        const cos = math.cos(angle_);
        const sin = math.sin(angle_);
        const x = this.x;
        const y = this.y;
        this.x = cos * x - sin * y;
        this.y = sin * x + cos * y;
    }

    pub fn move(this: *@This(), x: f32, y: f32) void {
        this.x += x;
        this.y += y;
    }

    pub fn distanceTo(this: *@This(), other: *@This()) @This() {
        return .{
            .x = other.x - this.x,
            .y = other.y - this.y,
        };
    }

    pub fn angle(this: *const @This()) f32 {
        return std.math.atan2(f32, this.x, this.y);
    }

    pub fn size(this: *@This()) f32 {
        return std.math.hypot(f32, this.x, this.y);
    }

};

test {
    var v = Vec2.new(1, 0);
    v.rotate(math.pi / 2.0);
    try testing.expectApproxEqAbs(@as(f32, 0), v.x, 0.001);
    try testing.expectApproxEqAbs(@as(f32, 1), v.y, 0.001);
    v.rotate(math.pi / 2.0);
    try testing.expectApproxEqAbs(@as(f32, -1), v.x, 0.001);
    try testing.expectApproxEqAbs(@as(f32, 0), v.y, 0.001);
    v.rotate(math.pi / 2.0);
    try testing.expectApproxEqAbs(@as(f32, 0), v.x, 0.001);
    try testing.expectApproxEqAbs(@as(f32, -1), v.y, 0.001);
    v.rotate(math.pi / 2.0);
    try testing.expectApproxEqAbs(@as(f32, 1), v.x, 0.001);
    try testing.expectApproxEqAbs(@as(f32, 0), v.y, 0.001);

    var v2 = Vec2.new(0, 0);
    try testing.expectApproxEqAbs(@as(f32, 0), v2.size(), 0.001);
    try testing.expectApproxEqAbs(@as(f32, 0), v2.size(), 0.001);

}

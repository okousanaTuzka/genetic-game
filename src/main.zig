const std = @import("std");
const SDL = @import("sdl2");
const render = @import("render.zig");
const sim = @import("sim.zig");
const Vec2 = @import("vector.zig").Vec2;
const perf = @import("perf.zig");
const nn = @import("nn.zig");
const ProximitySys = @import("systems/collisions.zig").ProximitySys;
const Collision = @import("systems/collisions.zig").Collision;

const WIDTH = 3800;
const HEIGHT = 2000;

pub fn main() !void {

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    var allocator = arena.allocator();

    try SDL.init(.{
        .video = true,
        .events = true,
        .audio = true,
    });
    defer SDL.quit();
    try SDL.ttf.init();
    defer SDL.ttf.quit();

    var window = try SDL.createWindow(
        "SDL.zig Basic Demo",
        .{ .centered = {} },
        .{ .centered = {} },
        3840,
        2160,
        .{ .vis = .shown },
    );
    defer window.destroy();

    var sdl_renderer = try SDL.createRenderer(window, null, .{ .accelerated = true });
    defer sdl_renderer.destroy();
    const renderer = try render.Renderer.new(sdl_renderer);

    var perf_meter = perf.PerfMeter.new(allocator);
    defer perf_meter.deinit();


    var movSys = sim.MovementSys{ .width = WIDTH, .height = HEIGHT };
    var collisionsSys = ProximitySys{ .allocator = allocator, .width = WIDTH, .height = HEIGHT, .block_size = 100, .perf_meter = &perf_meter };
    var brainSys = sim.BrainSys{ .width = WIDTH, .height = HEIGHT };
    var scoreSys = sim.ScoreSys{ .width = WIDTH, .height = HEIGHT, .max_population = 6_000 };
    var lifeTimeSys = sim.LifeTimeSys{};
    var renderSys = render.RendererSys.new(renderer);



    var entities = std.ArrayList(sim.base).init(allocator);
    defer entities.deinit();
    try init_entities(&entities);

    var timer = try std.time.Timer.start();
    mainLoop: while (true) {
        var loop_arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
        defer loop_arena.deinit();
        var loop_allocator = loop_arena.allocator();

        try renderer.clear();

        const update_time = @as(f32, @floatFromInt(timer.lap())) / 1_000_000_000.0;
        const delta_time: f32 = @min(0.1, update_time * 5);

        var buffer: [128:0]u8 = undefined;
        try perf_meter.addLabel(try std.fmt.bufPrintZ(&buffer, "update_time {d:.0} ms", .{ update_time * 1000 }));
        try perf_meter.addLabel(try std.fmt.bufPrintZ(&buffer, "delta_time  {d:.0} ms", .{ delta_time * 1000 }));
        try perf_meter.addLabel(try std.fmt.bufPrintZ(&buffer, "entities    {d:}", .{ entities.items.len }));


        while (SDL.pollEvent()) |ev| {
            switch (ev) {
                .quit => {
                    break :mainLoop;
                },
                .key_down => |key| {
                    switch (key.scancode) {
                        .escape => break :mainLoop,
                        else => std.log.info("key pressed: {}\n", .{key.scancode}),
                    }
                },
                .mouse_button_down => |mouse_but| {
                    switch (mouse_but.button) {
                        .left => clicked(@floatFromInt(mouse_but.x), @floatFromInt(mouse_but.y), &entities),
                        else => {}
                    }
                },

                else => {},
            }
        }

        var collisions = std.ArrayList(Collision).init(loop_allocator);

//         perf_meter.start("movement");
//         try movSys.calcMovement(delta_time, &entities);

        try perf_meter.run(sim.MovementSys, sim.MovementSys.calcMovement, .{&movSys, delta_time, &entities});

        perf_meter.start("collisions");
        try collisionsSys.calcCollisionsChunked(delta_time, &entities, &collisions);
        perf_meter.endStart("brain");
        try brainSys.calcDecision(delta_time, &entities, &collisions);

        perf_meter.endStart("render");
        try scoreSys.update(delta_time, &entities, &collisions);
        perf_meter.endStart("lifeTime");
        try lifeTimeSys.update(delta_time, &entities);

        perf_meter.endStart("render");
        try renderSys.render(delta_time, &entities);
        perf_meter.end();

        try perf_meter.renderResults(renderer);
        perf_meter.reset();
        renderer.show();

        std.time.sleep(10_000_000 -| timer.read());  // limit to 100 fps
    }
}

fn init_entities(entities: *std.ArrayList(sim.base)) !void {
    var r = std.rand.DefaultPrng.init(13);
    var rand = r.random();
    for (0..1000) |_| {
        const d = rand.float(f32) * std.math.pi * 2;
        var en = sim.base{
            .size = 5,
            .x = rand.float(f32) * WIDTH,
            .y = rand.float(f32) * HEIGHT,
            .dir_rad = d,
            .vel =  100,
            .rotation = rand.float(f32) - 0.5,
            .net = .{},
        };
        const coef = 1;
        en.net.setWeights(1, 0, &[_]f16{@floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef )});
        en.net.setWeights(1, 1, &[_]f16{@floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef )});
        en.net.setWeights(1, 2, &[_]f16{@floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef )});
        en.net.setWeights(1, 3, &[_]f16{@floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef )});
        en.net.setWeights(2, 0, &[_]f16{@floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef )});
        en.net.setWeights(2, 1, &[_]f16{@floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef )});
        en.net.setWeights(2, 2, &[_]f16{@floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef )});
        en.net.setWeights(2, 3, &[_]f16{@floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef ), @floatCast((rand.float(f32) - 0.5) * coef )});

        try entities.append(en);
//         std.log.info("{d:.2} {d:.2} {d:.2} {d:.2} {d:.2} {d:.2} {}", entities.getLast());
    }
    sim.updateEntityIds(entities);
}

fn clicked(x: f32, y: f32, entities: *std.ArrayList(sim.base)) void {
    var none_selected = true;
    for (entities.items) |*en| {
        if (none_selected
                and en.x - en.size < x
                and en.x + en.size > x
                and en.y - en.size < y
                and en.y + en.size > y)
        {
            none_selected = false;
            en.selected = true;
        } else {
            en.selected = false;
        }
    }
}

test {
     std.testing.refAllDeclsRecursive(@This());
     _ = nn.Layer(1, 1, .identity);
     _ = sim.base;
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}

const std = @import("std");
const sim = @import("sim.zig");
const Renderer = @import("render.zig").Renderer;


pub const PerfMeter = struct {

    allocator: std.mem.Allocator,
    timer: std.time.Timer,
    names: std.ArrayList([:0]const u8),
    times: std.StringArrayHashMap(u64),
    custom_labels: std.ArrayList([:0]const u8),


    pub fn new(allocator: std.mem.Allocator) @This() {
        return .{
            .allocator = allocator,
            .timer = std.time.Timer.start() catch unreachable,
            .names = std.ArrayList([:0]const u8).init(allocator),
            .times = std.StringArrayHashMap(u64).init(allocator),
            .custom_labels = std.ArrayList([:0]const u8).init(allocator),
        };
    }

    pub fn run(this: *@This(), comptime T: type, comptime fnc: *const fn(*T, f32, *std.ArrayList(sim.base)) anyerror!void, args: anytype) !void {
        const name = @typeName(T);
        this.start(name);
        const r = @call(.auto, fnc, args);
        this.end();
        return r;
    }

    pub fn start(this: *@This(), name: [:0]const u8) void {
        const time = this.timer.lap();
        if (this.names.items.len > 0) {
            const last_name = this.names.getLast();
            this.times.put(last_name, this.times.get(last_name).? + time) catch unreachable;
        }
        if (!this.times.contains(name)) {
            this.times.put(name, 0) catch unreachable;
        }
        this.names.append(name) catch unreachable;
    }

    pub fn end(this: *@This()) void {
        const name = this.names.orderedRemove(this.names.items.len - 1);
        const time = this.timer.lap();
        this.times.put(name, this.times.get(name).? + time) catch unreachable;
    }

    pub fn endStart(this: *@This(), name: [:0]const u8) void {
        this.end();
        this.start(name);
    }

    pub fn addLabel(this: *@This(), label: [:0]const u8) !void {
        try this.custom_labels.append(try this.allocator.dupeZ(u8, label));
    }

    pub fn reset(this: *@This()) void {
        this.names.clearAndFree();
        this.times.clearAndFree();
        for (this.custom_labels.items) |l| this.allocator.free(l);
        this.custom_labels.clearAndFree();
    }

    pub fn logResults(this: *@This()) void {
        for (this.times.keys(), this.times.values()) |k, v| {
            std.log.info("{s}: {d} us", .{ k, v / 1000 });
        }
    }

    pub fn renderResults(this: *@This(), renderer: Renderer) !void {
        var line: usize = 0;
        for (this.times.keys(), this.times.values()) |k, v| {
            try renderer.renderText(0, @intCast(32 * line), "{s}: {d} us", .{ k, v / 1000 });
            line += 1;
        }
        try renderer.renderText(0, @intCast(32 * line), "---------------------", .{});
        line += 1;
        for (this.custom_labels.items) |l| {
            try renderer.renderText(0, @intCast(32 * line), "{s}", .{ l });
            line += 1;
        }
    }

    pub fn deinit(this: *@This()) void {
        this.names.deinit();
        this.times.deinit();
        for (this.custom_labels.items) |l| this.allocator.free(l);
        this.custom_labels.deinit();
    }

};


test {
    var allocator = std.testing.allocator;
    var m = PerfMeter.new(allocator);
    defer m.deinit();

    m.start("a");
    m.end();
    m.start("a");
    m.start("b");
    m.end();
    m.end();

    m.logResults();
}

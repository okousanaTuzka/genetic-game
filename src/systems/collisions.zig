const std = @import("std");
const math = @import("std").math;
const Vec2 = @import("../vector.zig").Vec2;
const perf = @import("../perf.zig");
const utils = @import("../utils.zig");
const base = @import("../sim.zig").base;

pub const Collision = struct {
    entity1: *base,
    entity2: *base,
    distance: f32,
    dir: Vec2,
};

pub const ProximitySys = struct {
    allocator: std.mem.Allocator,
    width: f32,
    height: f32,
    block_size: f32,
    perf_meter: *perf.PerfMeter,

    pub fn calcCollisionsChunked(this: *@This(), delta_time: f32, entities: *std.ArrayList(base), collisions: *std.ArrayList(Collision)) !void {
        const nx: usize = @as(usize, @intFromFloat(try math.divCeil(f32, this.width, this.block_size) + 2));  // two extra row for eliminating branching at edgge
        const ny: usize = @as(usize, @intFromFloat(try math.divCeil(f32, this.height, this.block_size) + 2));  // two extra row for eliminating branching at edgge
        this.perf_meter.start("  chunks init");

//         this cause memory leak and i dont know why
//         var chunks: [][4096]?*base = try this.allocator.alloc([4096]?*base, nx * ny);
//         defer this.allocator.free(chunks);
//         var chunks_len = try this.allocator.alloc(u16, nx * ny);
//         defer this.allocator.free(chunks_len);
        var chunks: [][2048]?*base = @constCast(&(std.mem.zeroes([1024][2048]?*base)));
        var chunks_len = @constCast(&(std.mem.zeroes([1024]u16)));
        @memset(chunks_len, 0);
        for (entities.items) |*e| {
            const xi = @as(usize, @intFromFloat(e.x / this.block_size)) + 1;  // first column is in outer space
            const yi = @as(usize, @intFromFloat(e.y / this.block_size)) + 1;  // first row is in outer space
            const chunk_i = xi * ny + yi;
            chunks[chunk_i][chunks_len[chunk_i]] = e;
            chunks_len[chunk_i] += 1;
        }

        this.perf_meter.endStart("  calc collis");
        var count: usize = 0;
        for (1..nx-1) |xi| {
            for (1..ny-1) |yi| {
                const chunk_i = xi * ny + yi;
                for (0..chunks_len[chunk_i]) |ci1| {
                    const e1 = chunks[chunk_i][ci1].?;
                    const a: [5][2]isize = .{ .{0, 0}, .{1, 0}, .{-1, 1}, .{0, 1}, .{1, 1}};
                    for (a) |offsets| {
//                         const x: usize = @intCast(@as(isize, @intCast(xi)) + offsets[0]);
//                         const y = yi + offsets[1];
                        const offset: isize = offsets[0] * @as(isize, @intCast(ny)) + offsets[1];
                        const offseted_chunk_i: usize = @intCast(@as(isize, @intCast(chunk_i)) + offset);
                        for (0..chunks_len[offseted_chunk_i]) |ci2| {
                            const e2 = chunks[offseted_chunk_i][ci2].?;
                            count += 1;
                            if (e1 != e2) {
                                collisions.append(calcCollision(e1, e2)) catch unreachable;
                            }
                        }
                    }
                }
            }
        }
        var buffer: [128:0]u8 = undefined;
        const text = try std.fmt.bufPrintZ(&buffer, "collisions checks {d}", .{ count });
        try this.perf_meter.addLabel(text);

        this.perf_meter.endStart("  handle");
        this.handle(delta_time, collisions);
        this.perf_meter.end();
    }

    pub fn handle(this: *@This(), delta_time: f32, collisions: *std.ArrayList(Collision)) void {
        _ = this;
        for (collisions.items) |col| {
            const a = col.entity1.size + col.entity2.size;
            if (col.distance < a) {
                const force_factor = (a / ((col.distance / 3) + 0.01)) * delta_time;
                const x_ratio = @fabs(col.dir.x) / ((@fabs(col.dir.x) + @fabs(col.dir.y)) + 0.00001);
                const limit = (a - col.distance) / 2;
                const force_x = x_ratio * @min(limit, force_factor) * math.sign(-col.dir.x);
                const force_y = (1 - x_ratio) * @min(limit, force_factor) * math.sign(-col.dir.y);
                col.entity1.x += force_x;
                col.entity1.y += force_y;
                col.entity2.x -= force_x;
                col.entity2.y -= force_y;
            }
        }
    }

    fn isCollision(e1: *base, e2: *base) bool {
        const dist_x = e1.x - e2.x;
        const dist_y = e1.y - e2.y;
        const dist = math.hypot(f32, dist_x, dist_y);
        return dist < e1.size + e2.size;
    }

    fn calcCollision(e1: *base, e2: *base) Collision {
        const dist_x = e2.x - e1.x;
        const dist_y = e2.y - e1.y;
        const dist = math.hypot(f32, dist_x, dist_y);
        return .{
            .entity1 = e1,
            .entity2 = e2,
            .distance = dist,
            .dir = Vec2.new(dist_x, dist_y),
        };
    }
};


test "collisions_calculation" {

    var allocator = std.testing.allocator;

    var perf_meter = perf.PerfMeter.new(allocator);
    defer perf_meter.deinit();
    var collision_sys = ProximitySys{ .allocator = allocator, .width = 100, .height = 100, .block_size = 50, .perf_meter = &perf_meter };
    std.log.err("a", .{});

    var entities = std.ArrayList(base).init(allocator);
    defer entities.deinit();
    std.log.err("a", .{});

    var collisions = std.ArrayList(Collision).init(allocator);
    defer collisions.deinit();
    std.log.err("a", .{});

    try entities.append(.{ .x = 10, .y = 10, .size = 10});
    try entities.append(.{ .x = 10, .y = 80, .size = 10});
    try entities.append(.{ .x = 10, .y = 20, .size = 10});
    std.log.err("a", .{});

    var a = &entities;
    std.log.err("a1", .{});
    var b = &collisions;

    std.log.err("a1", .{});
    // call calcCollisionsChunked cause sigsegv but only in tests and calling itself not function body
    try collision_sys.calcCollisionsChunked(0.16, a, b);
    std.log.err("a1", .{});
//     try std.testing.expectEqual(@as(usize, 1), collisions.items.len);
//     try std.testing.expectEqual(@as(f32, 70), collisions.getLast().distance);


}

test "collisions_between_chunks" {

    var perf_meter = perf.PerfMeter.new(std.testing.allocator);
    defer perf_meter.deinit();
    var collision_sys = ProximitySys{ .allocator = std.testing.allocator, .width = 120, .height = 120, .block_size = 40, .perf_meter = &perf_meter };

    var entities = std.ArrayList(base).init(std.testing.allocator);
    defer entities.deinit();

    var collisions = std.ArrayList(Collision).init(std.testing.allocator);
    defer collisions.deinit();

    try entities.append(.{ .x =  60, .y =  60, .size = 10});
    try entities.append(.{ .x =  20, .y =  20, .size = 10});
    try entities.append(.{ .x = 100, .y =  20, .size = 10});
    try entities.append(.{ .x =  20, .y = 100, .size = 10});
    try entities.append(.{ .x = 100, .y = 100, .size = 10});

    try collision_sys.calcCollisionsChunked(0.16, &entities, &collisions);
    try std.testing.expectEqual(@as(usize, 4), collisions.items.len);
    entities.clearRetainingCapacity();
    collisions.clearRetainingCapacity();

    try entities.append(.{ .x =  20, .y =  60, .size = 10});
    try entities.append(.{ .x = 100, .y =  60, .size = 10});
    try entities.append(.{ .x =  60, .y =  20, .size = 10});
    try entities.append(.{ .x =  60, .y = 100, .size = 10});

    try collision_sys.calcCollisionsChunked(0.16, &entities, &collisions);
    try std.testing.expectEqual(@as(usize, 4), collisions.items.len);
    entities.clearRetainingCapacity();
    collisions.clearRetainingCapacity();

}

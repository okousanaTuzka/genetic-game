const std = @import("std");

pub const Activation = enum {
    identity,
    tanh,
    arctan,
};

pub fn NeuralNetwork(comptime topology: []const usize, comptime activations: []const Activation) type {
    comptime var tupleTypes: [topology.len-1]type = .{ type } ** (topology.len - 1);
    inline for (1..(topology.len)) |i| {
        tupleTypes[i-1] = Layer(topology[i-1], topology[i], activations[i-1]);
    }
    const layersType = comptime std.meta.Tuple(tupleTypes[0..]);

    return struct {
        layers: layersType = abc(),

        fn abc() layersType {
            comptime var a: layersType = undefined;
            inline for (1..(topology.len)) |i| {
                a[i-1] = Layer(topology[i-1], topology[i], activations[i-1]).new();
            }
            return a;
        }

        pub fn setWeights(this: *@This(), comptime layer: usize, neuron: usize, weights: []const f16) void {
            for (weights, 0..) |w, i| {
                // first layer is only input
                this.layers[layer-1].setWeight(neuron, i, w);
            }
        }

        pub fn calcInput(this: *@This(), input: [topology[0]]f16) [topology[topology.len-1]]f16 {
            var result: [*]const f16 = @constCast(&input);
            inline for (&(this.layers)) |*layer| {
                result = @ptrCast(@constCast(&(layer.calcInput(result[0..layer.input_count]))));
            }

            return @constCast(result[0..topology[topology.len-1]]).*;
        }

    };
}


pub fn Layer(comptime input_count: usize, comptime neuron_count: usize, comptime activation: Activation) type {
    return struct {
        comptime input_count: usize = input_count,
        comptime neuron_count: usize = neuron_count,
        comptime activation: Activation = activation,
        neurons: [neuron_count]@Vector(input_count+1, f16),

        pub fn new() @This() {
            return .{
                .neurons = [_]@Vector(input_count+1, f16){ ([_]f16{ 0 } ** (input_count+1)) } ** neuron_count,
            };
        }

        pub fn setWeight(this: *@This(), neuron: usize, input: usize, weight: f16) void {
            this.neurons[neuron][input] = weight;
        }

        pub fn calcInput(this: *@This(), input: *const [input_count]f16) [neuron_count]f16 {
            var input_: [input_count+1]f16 = undefined;
            input_[0] = 1;
            @memcpy(input_[1..], input);
            const vector: @Vector(input_count+1, f16) = input_;

            var result = [_]f16{0} ** neuron_count;
            inline for (0..result.len) |j| {
                result[j] = @reduce(.Add, vector * this.neurons[j]);

                switch (this.activation) {
                    inline .tanh => result[j] = @floatCast(std.math.tanh(@as(f32, result[j]))),
                    inline .arctan => result[j] = @floatCast(std.math.atan(@as(f32, result[j]))),
                    inline .identity => {},
                }
            }

            return result;
        }

    };
}


test "layer" {

    const a = Layer(2, 2, .identity).new();
    var l = @constCast(&a);

    const input = [_]f16{0.5, 0.2};
    l.setWeight(0, 0, 1);
    l.setWeight(0, 1, 1);
    l.setWeight(0, 2, 1);

    l.setWeight(1, 0, 0.4);
    l.setWeight(1, 1, 3);
    l.setWeight(1, 2, 10);

    try std.testing.expectEqualSlices(f16, &[_]f16{1.7, 3.9}, &l.calcInput(&input));

}

test "network" {

//     const a = NeuralNetwork(&[_]usize{2, 2, 1}, &[_]Activation{.identity, .identity}){};
//     var l = @constCast(&a);
//
//     var input = [_]f16{0.5, 0.2};
//     l.setWeights(1, 0, &[_]f16{0, 1, 1});
//     l.setWeights(1, 1, &[_]f16{1, 0.5, 0.5});
//
//     l.setWeights(2, 0, &[_]f16{0.5, 0.5, 1});
//
//     try std.testing.expectEqualSlices(f16, &[_]f16{2.2}, &l.calcInput(input));

}
